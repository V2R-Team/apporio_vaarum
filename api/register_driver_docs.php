<?php

include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

$driver_id=$_REQUEST['driver_id'];
 
$insurance_date=$_REQUEST['insurance_date'];
$insurance_expiry_date=$_REQUEST['insurance_expiry_date'];
$insurance_state=$_REQUEST['insurance_state'];
$rc_date=$_REQUEST['rc_date'];
$rc_expiry_date=$_REQUEST['rc_expiry_date'];
$rc_state=$_REQUEST['rc_state'];
$license_date=$_REQUEST['license_date'];
$license_expiry_date=$_REQUEST['license_expiry_date'];
$license_state=$_REQUEST['license_state'];
$language_id=$_REQUEST['language_id'];


if($driver_id!="" && $insurance_date != "" && $insurance_expiry_date != "" && $insurance_state != "" && $rc_date != "" && $rc_expiry_date != "" && $rc_state != "" && $license_date != "" && $license_expiry_date != "" && $license_state != "")
{
	if(!empty($_FILES['insurance'])) 
	{
		$img_name=$_FILES['insurance']['name'];
		$filedir="../uploads/driver/";
		
		if(!is_dir($filedir)) mkdir($filedir, 0755, true);
		{
			$fileext = strtolower(substr($_FILES['insurance']['name'],-4));
			if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
			{
				if($fileext=="jpeg") 
				{
					$fileext=".jpg";
				}
				$pfilename = time()."insurance_".$driver_id.$fileext;
				$filepath1 = "uploads/driver/".$pfilename;
				$filepath = $filedir.$pfilename;
				copy($_FILES['insurance']['tmp_name'], $filepath);
				$query1="UPDATE driver SET insurance='$filepath1', insurance_date_of_issue='$insurance_date', insurance_expiry_date='$insurance_expiry_date', insurance_state='$insurance_state' WHERE driver_id='$driver_id'";
				 
				 
	                        $db->query($query1);
				
				if(!empty($_FILES['license'])) 
				{
					$img_name=$_FILES['license']['name'];
					$filedir="../uploads/driver/";
		
					if(!is_dir($filedir)) mkdir($filedir, 0755, true);
					{
						$fileext = strtolower(substr($_FILES['license']['name'],-4));
						if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
						{
							if($fileext=="jpeg") 
							{
								$fileext=".jpg";
							}
							$pfilename = time()."license_".$driver_id.$fileext;
							$filepath1 = "uploads/driver/".$pfilename;
							$filepath = $filedir.$pfilename;
							copy($_FILES['license']['tmp_name'], $filepath);
							$query1="UPDATE driver SET license='$filepath1',licence_date_of_issue='$license_date',licence_expiry_date='$license_expiry_date',licence_state='$license_state' WHERE driver_id='$driver_id'";
							
	            			                $db->query($query1);
							
							if(!empty($_FILES['rc'])) 
							{
							 
								$img_name=$_FILES['rc']['name'];
								$filedir="../uploads/driver/";
		
								if(!is_dir($filedir)) mkdir($filedir, 0755, true);
								{
									$fileext = strtolower(substr($_FILES['rc']['name'],-4));
									if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
									{
										if($fileext=="jpeg") 
										{
											$fileext=".jpg";
										}
										$pfilename = time()."rc_".$driver_id.$fileext;
										$filepath1 = "uploads/driver/".$pfilename;
										$filepath = $filedir.$pfilename;
										copy($_FILES['rc']['tmp_name'], $filepath);
										$query1="UPDATE driver SET rc='$filepath1',rc_date_of_issue='$rc_date',rc_expiry_date='$rc_expiry_date',rc_state='$rc_state', detail_status= 2 WHERE driver_id='$driver_id'"; 
										 $db->query($query1);
										//print_r($query1);die();
										 
	            						

							
										$query3="select * from driver where driver_id='$driver_id'";
										$result3 = $db->query($query3);
										$list=$result3->row;

$car_type_id=$list['car_type_id'];
			$car_model_id=$list['car_model_id'];
			
			$query4="select * from car_type where car_type_id='$car_type_id'";
			$result4 = $db->query($query4);
			$list4=$result4->row;
			$car_type_name=$list4['car_type_name'];
			
			$query5="select * from car_model where car_model_id='$car_model_id'";
			$result5 = $db->query($query5);
			$list5=$result5->row;
			$car_model_name=$list5['car_model_name'];
$list['car_type_name']=$car_type_name;
			$list['car_model_name']=$car_model_name;

										$re = array('result'=> 1,'msg'=> "Signup Succesfully",'details'	=> $list);	
									}
								}
							}
							else 
							{
								$re = array('result'=> 0,'msg'=> "rc copy missing!!",);	
							}
						}
					}
				}
				else{
					$re = array('result'=> 0,'msg'=> "License copy missing!!",);	
				}
			}
		}
	}
	else
	{
		$re = array('result'=> 0,'msg'=> "Insurance copy missing!!",);	
	}
}
else
{
   $re = array('result'=> 0,'msg'=> "Required fields missing!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>