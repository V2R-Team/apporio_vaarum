<?php

function curl_get_contents($url, $verify_host = 0) {
	// echo $url;
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $verify_host);
    curl_setopt($ch, CURLOPT_SSLVERSION, "all");

	$data = curl_exec($ch);if ($data === FALSE) {
	    die("Curl failed: " . curl_error($ch));
	}
	curl_close($ch);
	
    return $data ? $data : $error;
}